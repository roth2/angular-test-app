FROM node:12-alpine AS build

RUN mkdir -p /app

WORKDIR /app

COPY package*.json /app/

RUN CYPRESS_INSTALL_BINARY=0 PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true npm install

COPY . /app/

RUN npm run build -- --prod

FROM nginxinc/nginx-unprivileged:1.16-alpine

COPY --from=build /app/dist /usr/share/nginx/html

COPY docker/stub_status.conf /etc/nginx/conf.d/stub_status.conf
