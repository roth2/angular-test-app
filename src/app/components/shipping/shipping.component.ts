import { Component, OnInit } from '@angular/core';
import { CartService } from '../../services/cart.service';
import { ShippingPrice } from '../../products';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
})
export class ShippingComponent implements OnInit {
  shippingCosts: ShippingPrice[] = [];

  constructor(private cartService: CartService) {}

  ngOnInit() {
    this.cartService.getShippingPrices().subscribe(shippingCosts => {
      this.shippingCosts = shippingCosts;
    });
  }
}
